<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ServiceController extends Controller
{
    public function index(){
        $services =Service::get();
        return view("admin.services.list" ,compact('services'));
    }

    public function create()
    {
        return view("admin.services.add");
    }

    public function store(Request $request)
    {
        // Service::create([
        //     'title' => $request ->title ,
        //     'content' => $request ->content,
        //     'content'=>$path,
        //     'icon' => $request ->icon
        // ]);

            // $image = $request ->file('img')->getClientOriginalName() ;
            // $path= $request ->file('img')->storeAs('services', $image ,'service');
            // Service::create([
            //         'title' => $request ->title ,
            //         'content' => $request ->content,
            //         'imgurl'=>$path,
            //         'icon'  =>$request->icon,
            // ]);
            // return redirect()->route('service.index');
            
                $path = 'images/';
                $file = $request->file;
                $imageExtension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $imageExtension;
                $filename = $path.$filename;
                $file->move($path, $filename);
         
             Service::create([
                    'title' => $request ->title ,
                   'content' => $request ->content,
                   'imgurl'    =>$filename,
                   'icon'  =>$request->icon,
            ]);
            return redirect()->route('service.index');

    }


  
    public function show($id)
    {
        $services = Service::findorfail($id);
        return view('admin.services.show' , compact('services'));
    }


    public function edit($id)
    {
        $services = Service::findorfail($id);
        return view('admin.services.edit' , compact('services'));
    }


    public function update(Request $request ,$id)
    {
    //   $services = Service::find($id);
    //   $services->title = $request->title;
    //   $services->content = $request->content;
    //   $services->img = $request->img;
    //   $services->icon = $request->icon;
    //   $services->save();
    //   return redirect()->route('service.index');
    // }

    $services = Service::find($id);
  
    if($request->file != ''){
        (File::exists($services->imgurl)) ? File::delete($services->imgurl) : Null;
        $path = 'images/';

        $file = $request->file;
        $imageExtension = $file->getClientOriginalExtension();
        $filename = time() . '.' . $imageExtension;
        $filename = $path.$filename;
        $file->move($path, $filename);

        $services->update([
            'imgurl' =>$filename
        ]);
    }

              
              
                    $services->update([
                      
                            'title' =>$request->title,
                            'content' => $request->content,
                            'icon'  =>$request->icon
                        ]);
                        return redirect()->route('service.index');

               
        }
    public function destroy($id)
    {
        Service::find($id)->delete();
        return redirect()->route('service.index');//
    }
}
