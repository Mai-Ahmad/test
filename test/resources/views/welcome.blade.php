@extends('layouts.header')

@section('content')
<!--begin team section -->
<section class="section-grey" id="services">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-title">خدماتنا</h2>
            </div>
            @foreach($getser as $getsers)
            <div class="col-sm-12 col-md-4 margin-top-30">
                <img src="{{$getsers->imgurl}}" class="team-img width-100"
                    alt="pic">

                <div class="team-item">
                    <h3>{{$getsers->title}}</h3>
                    <p>{{$getsers->content}}</p>
                    <i class="{{$getsers->icon}}"></i>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!--end team section-->

@endsection

