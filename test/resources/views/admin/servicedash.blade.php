<!DOCTYPE html>
<html lang="en">


<body class="">

    <!-- BEGIN LOADER -->
    <div id="load_screen">
        <div class="loader">
            <div class="loader-content">
                <div class="spinner-grow align-self-center"></div>
            </div>
        </div>
    </div>

    <!-- navbar -->
    @include('admin.layouts.nav')
    <div class="main-container " id="container">
        <div class="overlay"></div>
        <div class="search-overlay"></div>
        @include('admin.layouts.sidebar')
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="middle-content container-xxl p-0">
                    @include('admin.layouts.secoundbar')
                    <!--  BEGIN CONTENT AREA  -->
                    {{--@include('admin.layouts.content')--}}
                    @yield('content')
                    <!--  END CONTENT AREA  -->
                </div>
            </div>
        </div>
    </div>
    @include('admin.layouts.script')
</body>
</html>
