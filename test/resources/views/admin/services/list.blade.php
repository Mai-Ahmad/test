<!DOCTYPE html>
<html lang="en">
@include('admin.layouts.headlist')



@extends('admin.servicedash')

@section('content')
<div class="row" id="cancel-row">

    <div class="col-xl-12 col-lg-12 col-sm-12 layout-top-spacing layout-spacing">
        <div class="widget-content widget-content-area br-8">
            <table id="invoice-list" class="table dt-table-hover" style="width:100%">
                <button class="dt-button btn btn-primary _effect--ripple waves-effect waves-light"><a
                        href="{{Route('service.create')}}">إضافة خدمة</a></button>
                <thead>
                    <tr>
                        <th class="checkbox-column"></th>
                        <th>اسم الخدمة</th>
                        <th>المحتوى</th>
                        <th>الايقونة</th>
                        <th>العملية</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($services as $service)
                    <tr>
                        <td>{{$service->id}}</td>

                        <td class="checkbox-column">
                            <div class="d-flex">
                                <div class="usr-img-frame me-2 rounded-circle">
                                    <img alt="avatar" class="img-fluid rounded-circle" src="/{{$service->imgurl}}">
                                </div>
                                <p class="align-self-center mb-0 user-name">{{$service->title}}</p>
                            </div>
                        </td>
                        <td>{{$service->content}}</td>
                        <td>{{$service->icon}}</td>
                        <td>
                            <a class="badge badge-light-primary text-start me-2 action-edit"
                                href="{{Route('service.show' , $service->id)}}"><i class="fas fa-eye"></i></a>
                            <a class="badge badge-light-primary text-start me-2 action-edit"
                                href="{{Route('service.edit' , $service->id)}}"><svg xmlns="http://www.w3.org/2000/svg"
                                    width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                    stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-add-3">
                                    <path d="M12 20h9"></path>
                                    <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z">
                                    </path>
                                </svg></a>

                            <form action="{{Route('service.destroy' , $service->id)}}" method="post" class=inline>
                                @method('DELETE')
                                @csrf
                                <button type=submit class="badge badge-light-primary text-start me-2 action-edit"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-trash">
                                        <polyline points="3 6 5 6 21 6"></polyline>
                                        <path
                                            d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                        </path>
                                    </svg>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>


<!--  END CONTENT AREA  -->

@endsection
