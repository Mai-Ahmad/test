<!DOCTYPE html>
<html lang="en">
@include('admin.layouts.head')


@extends('admin.servicedash')

@section('content')

<div class="row mb-4 layout-spacing layout-top-spacing">
    <div class="col-xxl-9 col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="widget-content widget-content-area ecommerce-create-section">
            <form action="{{Route('service.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row mb-4">
                    <div class="col-sm-12">
                        <input class="form-control" placeholder="اسم الخدمة" type="text" name="title">
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-sm-12">
                        <label>المحتوى</label>
                        <div><input type="text" name="content" class="form-control" placeholder="Service content"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8">
                        <input type="file" name="file" class="form-control">
                    </div>

                    <div class="col-md-4">
                        <label for="product-images">اختار ايقونة</label>
                        <button name="icon" class="btn btn-secondary" role="iconpicker"></button>
                    </div>
                </div>
                <br>
                <button type="submit"
                    class="dt-button btn btn-primary _effect--ripple waves-effect waves-light">submit</button>
            </form>
        </div>
    </div>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    @endsection
    
    @include('admin.layouts.script')
</body>
</html>