<!DOCTYPE html>
<html lang="en">
@include('admin.layouts.head')

@extends('admin.servicedash')

@section('content')
<img src="/{{$services->imgurl}}"
 alt="">
<h3>{{$services->title}}</h3>
<p>{{$services->content}}</p>
<i style="color:red" class="{{$services->icon}}"></i>

@endsection
@include('admin.layouts.script')
</body>
</html>