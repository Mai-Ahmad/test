<!DOCTYPE html>
<html lang="en">
@include('admin.layouts.head')

@extends('admin.servicedash')

@section('content')

<div class="row mb-4 layout-spacing layout-top-spacing">

    <div class="col-xxl-9 col-xl-12 col-lg-12 col-md-12 col-sm-12">

        <div class="widget-content widget-content-area ecommerce-create-section">
            <form action="{{Route('service.update', $services->id)}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row mb-4">
                    <div class="col-sm-12">
                        <input type="text" name="title" value="{{$services->title}}" class="form-control">
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-sm-12">
                        <label>المحتوى</label>
                        <div>
                            <input type="text" name="content" value="{{$services->content}}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <label for="product-images">ارفع الصورة</label>
                        <div class="multiple-file-upload">
                            
                            <input type="file" class="filepond file-upload-multiple" name="file"
                                >
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="product-images">اختار ايقونة</label>
                        <button name="icon" class="btn btn-secondary" role="iconpicker"></button>
                    </div>
                </div>
                <br><br>
                <button type="submit"
                    class="dt-button btn btn-primary _effect--ripple waves-effect waves-light">update</button>
            </form>
        </div>
    </div>
</div>


@endsection
@include('admin.layouts.script')
</body>
</html>