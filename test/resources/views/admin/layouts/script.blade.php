</div>
            </div>
        </div>
    </div>


<script type="text/javascript">

$(function(){
    var show_result = function(){
        $('#result').text($('#button').html().trim());
    };

    show_result();

    $('#btn-text').on('focusout', function(e) {
        $('#btn-icon-positions button[value="' + $('#button').data('position') + '"]').trigger('click')
        show_result();
    });

    $('#btn-colors button').on('click', function(e) {
        $('#button a').removeClass('btn-primary btn-secondary btn-success btn-danger btn-warning btn-info btn-light btn-dark btn-link').addClass($(this).val());
        show_result();
    });

    $('#btn-sizes button').on('click', function(e) {
        $('#button a').removeClass('btn-sm btn-lg').addClass($(this).val());
        show_result();
    });

    $('#btn-sizes a').on('click', function(e) {
        $('#button a').toggleClass('btn-block');
        show_result();
    });

    $('#btn-icon').iconpicker({
        rows: 5,
        cols: 10,
        align: 'left'
    });

    $('#btn-icon').on('change', function(e) {
        $('#button a > i').attr('class', '').addClass(e.icon);
        show_result();
    });

    $('#btn-icon-positions button').on('click', function(e) {
        var icon = $('#button a > i');
        var text = $('#btn-text').val();
        $('#button a').empty();
        if($(this).val() == 'left'){
            $('#button a').append(icon).append(' ' + text);
        }
        else{
            $('#button a').append(text + ' ').append(icon);
        }
        $('#button').data('position', $(this).val());
        show_result();
    });
});

</script>

<script type="text/javascript">

$(function(){
    $('#btnDonate').bind('click', function(e){
        e.preventDefault();
        $('#formDonate').submit();
    });

    $.getJSON( "https://api.github.com/repos/victor-valencia/bootstrap-iconpicker", function( data ) {
        $('#btnStars').html(data.stargazers_count);
        $('#btnForks').html(data.forks_count);
    });

    $.getJSON( "https://api.github.com/repos/victor-valencia/bootstrap-iconpicker/tags", function( data ) {
        $('#btnReleases').html(data.length);

        var url = "https://github.com/victor-valencia/bootstrap-iconpicker/archive/" + data[0].name;

        $('#btnGithub').html($('#btnGithub').html().replace('{0}', data[0].name));

        $('#btnDownloadZip').attr('href', url + '.zip');
        $('#btnDownloadZip').html($('#btnDownloadZip').html().replace('{0}', data[0].name));

        $('#btnDownloadTar').attr('href', url + '.tar.gz');
        $('#btnDownloadTar').html($('#btnDownloadTar').html().replace('{0}', data[0].name));
    });

    $.getJSON( "https://api.github.com/repos/victor-valencia/bootstrap-iconpicker/contributors", function( data ) {
        $('#btnContributors').html(data.length);
    });

    $('[role="menu"] a').on('click', function(){
        $("#tabConfig").html($(this).html() + ' <span class="caret"></span>');
    });
});
</script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-38890641-4', 'auto');
ga('send', 'pageview');
</script>

<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="{{asset('icons/dist/js/bootstrap-iconpicker.bundle.min.js')}}"></script>



    <script src="{{asset('src/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/mousetrap/mousetrap.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/waves/waves.min.js')}}"></script>
    <script src="{{asset('vertical-light-menu/app.js')}}"></script>
    <script src="{{asset('src/plugins/src/highlight/highlight.pack.js')}}"></script>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('src/plugins/src/editors/quill/quill.js')}}"></script>
    <script src="{{asset('src/plugins/src/filepond/filepond.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/filepond/FilePondPluginFileValidateType.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/filepond/FilePondPluginImageExifOrientation.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/filepond/FilePondPluginImagePreview.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/filepond/FilePondPluginImageCrop.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/filepond/FilePondPluginImageResize.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/filepond/FilePondPluginImageTransform.min.js')}}"></script>
    <script src="{{asset('src/plugins/src/filepond/filepondPluginFileValidateSize.min.js')}}"></script>

    <script src="{{asset('src/plugins/src/tagify/tagify.min.js')}}"></script>

    <script src="{{asset('src/assets/js/apps/ecommerce-create.js')}}"></script>


    
<script src="{{asset('src/plugins/src/global/vendors.min.js')}}"></script>
   
    <script src="{{asset('src/assets/js/custom.js')}}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="{{asset('src/plugins/src/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('src/plugins/src/table/datatable/button-ext/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('src/assets/js/apps/invoice-list.js')}}"></script>

    <!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>