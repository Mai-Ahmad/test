<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     

        DB::table('users')->insert([
            'name'=>'Admin',
            'email'=>'mai@gmail.com',
             'is_admin'=>'1',
            'password'=> bcrypt('12345678'),
        ],
        [
           'name'=>'Regular User',
           'email'=>'hiba@gmail.com',
            'is_admin'=>'0',
           'password'=> bcrypt('12345678'),
        ],);
  
 
    }
}
